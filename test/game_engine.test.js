const GameEngine = require("../lib/game_engine");

describe("The Game Engine must", () => {

  test("start a new game with a random word", () => {
    const game_state = GameEngine.startGame(1);
    console.log(game_state);
    expect(game_state.display_word.length).toEqual((game_state.word.length)*2)
  });

  test("update the guesses and lives when a wrong guess is given", () => {
    let game_state = GameEngine.startGame(1);
    game_state.word = "siemens"; 
    game_state = GameEngine.takeGuess(game_state, "a", "P");
    expect(game_state.lives).toBe(4);
    expect(game_state.guesses.length).toBe(1);
  });

  test("update the guesses and the display word when a guess is right", () => {
    let game_state = GameEngine.startGame(1);
    game_state.word = "siemens";
    game_state.display_word = "_ _ _ _ _ _ _"
    game_state = GameEngine.takeGuess(game_state, "s", "P");

    console.log(game_state);

    expect(game_state.display_word).toEqual("s _ _ _ _ _ s")
    expect(game_state.guesses.length).toBe(1);

  });

  test("confirm victory", () => {
	let game_state = GameEngine.startGame(1);
	game_state.word = "siemens";
    game_state.display_word = "_ _ _ _ _ _ _"
    game_state = GameEngine.takeGuess(game_state, "s", "P");
    game_state = GameEngine.takeGuess(game_state, "i", "P");
    game_state = GameEngine.takeGuess(game_state, "e", "P");
    game_state = GameEngine.takeGuess(game_state, "m", "P");
    game_state = GameEngine.takeGuess(game_state, "n", "P");
    expect(game_state.display_word).toEqual("s i e m e n s")
    expect(game_state.lives).toEqual(5)
    expect(game_state.status).toBe("win!!!!!");
  });
  
  test("confirm victory with loss of lives", () => {
	let game_state = GameEngine.startGame(1);
	game_state.word = "siemens";
    game_state.display_word = "_ _ _ _ _ _ _"
    game_state = GameEngine.takeGuess(game_state, "s", "P");
    game_state = GameEngine.takeGuess(game_state, "i", "P");
    game_state = GameEngine.takeGuess(game_state, "e", "P");
    game_state = GameEngine.takeGuess(game_state, "k", "P");
    game_state = GameEngine.takeGuess(game_state, "g", "P");
    game_state = GameEngine.takeGuess(game_state, "j", "P");
    game_state = GameEngine.takeGuess(game_state, "w", "P");
    game_state = GameEngine.takeGuess(game_state, "n", "P");
    game_state = GameEngine.takeGuess(game_state, "m", "P");
    expect(game_state.display_word).toEqual("s i e m e n s")
    expect(game_state.lives).toEqual(1)
    expect(game_state.status).toBe("win!!!!!");
  });
  
  test("confirm victory with loss of lives", () => {
	let game_state = GameEngine.startGame(1);
	game_state.word = "siemens";
    game_state.display_word = "_ _ _ _ _ _ _"
    game_state.lives = 1;
    game_state = GameEngine.takeGuess(game_state, "w", "P");
    expect(game_state.display_word).toEqual("_ _ _ _ _ _ _")
    expect(game_state.lives).toEqual(0)
    expect(game_state.status).toBe("loooserrrrrrrrrrrrrr!");
  });
  
  test("get a tip", () => {
	  let game_state = GameEngine.startGame();
	  game_state.word = "siemens";
	  game_state.display_word = "_ _ _ _ _ _ _"
	  game_state = GameEngine.takeGuess(game_state, "w", "P");
	  game_state = GameEngine.takeGuess(game_state, "s", "P");
	  game_state = GameEngine.takeGuess(game_state, "e", "P");	
	  game_state = GameEngine.takeGuess(game_state, "i", "P");
	  game_state = GameEngine.takeGuess(game_state, "m", "P");
	  expect(GameEngine.takeGuess(game_state, "e", "D")).toBe("n");	  
  });
  
  test("User chooses \"easy\" difficulty", () => {
	let game_state = GameEngine.startGame(1);
    expect(game_state.lives).toEqual(5);
  });
  
  test("User chooses \"medium\" difficulty", () => {
	let game_state = GameEngine.startGame(2);
    expect(game_state.lives).toEqual(3);
  });
  
  test("User chooses \"hard\" difficulty", () => {
	let game_state = GameEngine.startGame(3);
    expect(game_state.lives).toEqual(1);    
  });
   
  test("User chooses \"easy\" and fails 4 times", () =>{
	  let game_state = GameEngine.startGame(1);
	  game_state.word = "siemens";
	  game_state.display_word = "_ _ _ _ _ _ _"
	  game_state = GameEngine.takeGuess(game_state, "w", "P");
	  game_state = GameEngine.takeGuess(game_state, "u", "P");
	  game_state = GameEngine.takeGuess(game_state, "o", "P");
	  game_state = GameEngine.takeGuess(game_state, "l", "P");
	  expect(game_state.doll).toBe("O-<--");
  });
 
  test("Score for \"easy\" game with 3-letter word", () => {
	let game_state = GameEngine.startGame(1);
	game_state.word = "CSD";
	game_state.display_word = "_ _ _"
	game_state = GameEngine.takeGuess(game_state, "C", "P");
	game_state = GameEngine.takeGuess(game_state, "S", "P");
	game_state = GameEngine.takeGuess(game_state, "D", "P");
    expect(game_state.score).toEqual(3);    
  });
 
  test("Score for \"medium\" game with 3-letter word", () => {
	let game_state = GameEngine.startGame(2);
	game_state.word = "CSD";
	game_state.display_word = "_ _ _"
	game_state = GameEngine.takeGuess(game_state, "C", "P");
	game_state = GameEngine.takeGuess(game_state, "S", "P");
	game_state = GameEngine.takeGuess(game_state, "D", "P");
    expect(game_state.score).toEqual(6);    
  });
 
  test("Score for \"hard\" game with 3-letter word", () => {
	let game_state = GameEngine.startGame(3);
	game_state.word = "CSD";
	game_state.display_word = "_ _ _"
	game_state = GameEngine.takeGuess(game_state, "C", "P");
	game_state = GameEngine.takeGuess(game_state, "S", "P");
	game_state = GameEngine.takeGuess(game_state, "D", "P");
    expect(game_state.score).toEqual(9);    
  });
});
