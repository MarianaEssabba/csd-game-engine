const random_line = require("@messabba/random-words");

const startGame = (level) => {
  var wordRandom = random_line.getRandomWord();
  return {
    status: "RUNNING",
    word: wordRandom,
    lives: setLives(level),
    display_word: buildDisplayWord(wordRandom),
    difficulty: level,
    doll: "",
    guesses: [],
	score: 0
  };
};

const takeGuess = (game_state, guess, action) => {
	
	if( action == "P" ) {		
		play( game_state, guess );
		checkStatus( game_state );	 
		return game_state;
	}
	if( action == "D" ) {
		return getLettersMissing( game_state );
	}
};

function setLives(level) {
	switch(level) {
		//easy
		case 1:
			return 5;
		//medium
		case 2:
			return 3;
		//hard
		case 3:
			return 1;
	}
}

function buildDisplayWord(word){
  var myWord ='';
  for(i=0; i<word.length; i++){
    myWord += '_ '
  }
  return myWord;
}

function buildWordWithGuess(guess, game_state){
  let word = game_state.word;
  let dw = game_state.display_word.split(" ");
  for(var i=0; i<word.length;i++) {
    if (word[i] === guess){
      dw[i]= guess;

    } 
  }
  game_state.display_word = dw.join(" ");
  return game_state.display_word;
    
}

function setCharAt(str,index,chr) {
    if(index > str.length-1) return str;
    return str.substr(0,index) + chr + str.substr(index+1);
}

function checkStatus( game_state ) {
	if(	game_state.display_word.indexOf("_") < 0 ) {
		game_state.status = "win!!!!!";
		game_state.score = getScore( game_state );
	}
	if(game_state.lives == 0 ) {
		game_state.status = "loooserrrrrrrrrrrrrr!";
	} 
}

function play(game_state, guess) {
	let is_a_wrong_guess = game_state.word.indexOf(guess) < 0;
	game_state.guesses.push(guess);
	if(is_a_wrong_guess){
		game_state.lives -= 1;
		game_state.doll = buildDoll( game_state );
	} else{
		let newDisplayWord = buildWordWithGuess(guess, game_state)
		return{
		  ...game_state,
		  display_word: newDisplayWord
		}
	}	
}

function getLettersMissing( game_state ) {
	
	var index = 0;
	var return_array = [];
	var word_array = game_state.word.split("");
	for(i = 0; i < game_state.word.length; i++) {	
		if( game_state.display_word.indexOf( word_array[i] ) < 0 ) {
			return_array[index] = word_array[i];
			index += 1;
		}
	}
	var arrayRandom = Math.floor(Math.random() * return_array.length); 
	return return_array[arrayRandom];	
}

function buildDoll( game_state ) {
	switch(game_state.difficulty) {
		//easy
		case 1:
			switch(game_state.doll) {
				case "":
					game_state.doll = "O-"; break;
				case "O-":
					game_state.doll = "O-<"; break;
				case "O-<":
					game_state.doll = "O-<-"; break;
				case "O-<-":
					game_state.doll = "O-<--"; break;
				case "O-<--":
					game_state.doll = "O-<--<"; break;
			}
		break;
		//medium
		case 2:
			switch(game_state.doll) {
				case "":
					game_state.doll = "O-"; break;
				case "O-":
					game_state.doll = "O-<-"; break;
				case "O-<-":
					game_state.doll = "O-<--<"; break;
			}
		break;
		//hard
		case 3:
			game_state.doll = "O-<--<"; break;
	}
	return game_state.doll;
}

function getScore( game_state ) {
	game_state.score = game_state.difficulty*game_state.word.length;
	return game_state.score;
}

module.exports = {
  startGame,
  takeGuess
};